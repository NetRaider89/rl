#!/usr/bin/env python

from distutils.core import setup

setup(name='rl',
      version='0.1',
      description='reinforcement learning package',
      author='Michele Fratello',
      author_email='f.michele89@yahoo.it',
      url='https://www.python.org/sigs/distutils-sig/',
      packages=['rl', 'rl.algorithm', 'rl.environment', 'rl.function']
     )
