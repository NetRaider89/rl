import warnings
from math import sqrt

class ValueIteration(object):
    def __init__(self, gamma=0.9, max_iter=100, tol=1e-3, verbose=1):
        self.gamma = gamma
        self.max_iter = max_iter
        self.tol = tol
        self.verbose = verbose
    
    def partial_fit(self, environment, function):
        # one iteration of the value iteration algorithm
        delta = 0
        cnt = 0
        to_visit = set(environment.initial_states())
        while len(to_visit):
            state = to_visit.pop()
            if environment.terminal(state):
                continue
            actions = environment.actions(state)
            for a in actions:
                a_value = 0
                for ss, p, r in environment.transition_reward(state, a):
                    to_visit.add(ss)
                    if environment.terminal(ss):
                        a_value += p * r
                    else:
                        a_value += p * (r + self.gamma * max(
                            [function.value(ss, aa) 
                                for aa in environment.actions(ss)]
                            ))
                delta += (function.value(state, a) - a_value) ** 2
                cnt += 1
                function.update(state, a, a_value)
        return sqrt(delta) / cnt
    
    def fit(self, environment, function):
        for i in range(self.max_iter):
            delta = self.partial_fit(environment, function)
            if self.verbose > 0:
                print 'Iteration {}: Average value difference {}'.format(
                    i + 1, round(delta, 5)                
                )
            if delta < self.tol:
                return
        warnings.warn('Did not converged in {} iterations'.format(self.max_iter))
