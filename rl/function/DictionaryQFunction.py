from .BaseFunction import BaseFunction

class DictionaryQFunction(BaseFunction):
    def __init__(self, init_value=None):
        self.function = dict()
        if init_value is None:        
            init_value = 0
        self.init_value = init_value
    
    def value(self, state, action):
        return self.function.get((state, action), self.init_value)
        
    def update(self, state, action, value):
        self.function[(state, action)] = value