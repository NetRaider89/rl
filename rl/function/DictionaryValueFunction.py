from .BaseFunction import BaseFunction

class DictionaryValueFunction(BaseFunction):
    def __init__(self, init_value=None):
        self.function = dict()
        if init_value is None:        
            init_value = 0
        self.init_value = init_value
    
    def value(self, state):
        return self.function.get(state, self.init_value)
        
    def update(self, state, value):
        self.function[state] = value