import abc

class BaseFunction(object):
    __metaclass__ = abc.ABCMeta
    
    @abc.abstractmethod
    def value(self):
        '''Returns the value of the state.'''
    @abc.abstractmethod
    def update(self):
        '''Updates the values of the policy.'''