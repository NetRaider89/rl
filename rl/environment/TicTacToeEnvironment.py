from rl.environment import BaseEnvironment

class TicTacToeEnvironment(BaseEnvironment):
    # The environment assumes that the agent is identiied by O, whereas the
    # opponent is X

    def initial_states(self):
        clear = '_' * 9
        init = [clear]
        for a in self.actions(clear):
            s = list(clear)
            s[a] = 'X'
            init.append(''.join(s))
        return init

    def winner(self, state, player):
        # check horizontal 
        return any((state[0] == state[1] == state[2] == player,
                    state[3] == state[4] == state[5] == player,
                    state[6] == state[7] == state[8] == player,
        # check vertical                   
                    state[0] == state[3] == state[6] == player,
                    state[1] == state[4] == state[7] == player,
                    state[2] == state[5] == state[8] == player,
        # check diagonal
                    state[0] == state[4] == state[8] == player,
                    state[2] == state[4] == state[6] == player))
    
    def draw(self, state):
        return not '_' in state
    
    def terminal(self, state):
        return any((self.winner(state, 'O'), self.winner(state, 'X'), 
                    self.draw(state)))
    
    def valid(self, state, action):
        return state[action] == '_'
        
    def actions(self, state):
        return tuple(pos for pos, s in enumerate(state) if s == '_')
        
    def transition(self, state, action):
        if not self.valid(state, action):
            raise ValueError('Invalid action!')
        state = list(state)
        state[action] = 'O'
        state = ''.join(state)
        if self.terminal(state):
            return [(state, 1)]
        next_states = []
        opponent_actions = self.actions(state)
        for a in opponent_actions:
            ss = list(state)
            ss[a] = 'X'
            next_states.append((''.join(ss), float(1) / len(opponent_actions)))
        return next_states
    
    def reward(self, state):
        if self.winner(state, 'O'):
            return 1
        if self.winner(state, 'X'):
            return -1
        return 0        