import abc

class BaseEnvironment(object):
    __metaclass__ = abc.ABCMeta
    
    @abc.abstractmethod
    def initial_states(self):
        '''Generates a list of initial states.'''
    
    @abc.abstractmethod
    def terminal(self, state):
        '''Given a state, check wether it is a terminal state.'''

    @abc.abstractmethod
    def actions(self, state):
        '''Given a state, return the available actions.'''        
    
    @abc.abstractmethod
    def transition(self, state, action):
        '''Given a state and a valid action, return the new states along 
           their corresponding probability'''
        
    @abc.abstractmethod
    def reward(self, state):
        '''Given a state, return the reward of being in given state.'''
    
    def transition_reward(self, state, action):
        '''Given a state and a valid action, return the new state and the 
        reward of being in that state, or the same state and 0 reward if 
        the state is a terminal state.'''
        ret = []
        if not self.terminal(state):        
            next_states = self.transition(state, action)
            for s, p in next_states:
                ret.append((s, p, self.reward(s)))
        else:
            ret.append((state, 1, 0))
        return ret